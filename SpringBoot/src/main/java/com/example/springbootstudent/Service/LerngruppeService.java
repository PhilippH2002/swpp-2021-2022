package com.example.springbootstudent.Service;

import com.example.springbootstudent.Entities.Student;
import com.example.springbootstudent.Repositories.LerngruppeRepo;
import com.example.springbootstudent.Repositories.StudentRepo;
import com.example.springbootstudent.Request.LerngruppeRequest;
import com.example.springbootstudent.Entities.Lerngruppe;
import org.springframework.stereotype.Service;

@Service
public class LerngruppeService
{
    private final LerngruppeRepo lerngruppeRepo;
    private final StudentRepo studentRepo;

    public LerngruppeService(LerngruppeRepo lerngruppeRepo, StudentRepo studentRepo)
    {
        this.lerngruppeRepo = lerngruppeRepo;
        this.studentRepo = studentRepo;
    }

    public String erzeugeLerngruppe(LerngruppeRequest lerngruppeRequest, int id)
    {
        Student student = studentRepo.findById(id);

        Lerngruppe lerngruppe = new Lerngruppe(lerngruppeRequest.getName(), lerngruppeRequest.getBeschreibung());
        lerngruppe.erzeuger(student);
        lerngruppeRepo.save(lerngruppe);

        return "Lerngruppe wurde erfolgreich angelegt";
    }

    public String studentHinzufugen(int gruppenId, int studentenId)
    {
        Lerngruppe lerngruppe = lerngruppeRepo.findById(gruppenId);
        Student student = studentRepo.findById(studentenId);
        lerngruppe.gruppeStudenten(student);
        lerngruppeRepo.save(lerngruppe);

        return "Der Student wurde erfolgreich hinzugefügt";
    }
}
