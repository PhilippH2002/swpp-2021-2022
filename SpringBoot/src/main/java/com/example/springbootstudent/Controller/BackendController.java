package com.example.springbootstudent.Controller;

import com.example.springbootstudent.Request.LoginRequest;
import com.example.springbootstudent.Request.LerngruppeRequest;
import com.example.springbootstudent.Request.StudentRequest;
import com.example.springbootstudent.Service.LerngruppeService;
import com.example.springbootstudent.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/test/v1")
public class BackendController
{
    @Autowired
    private final StudentService studentService;
    private final LerngruppeService lerngruppeService;

    public BackendController(StudentService studentService, LerngruppeService lerngruppeService) {
        this.studentService = studentService;
        this.lerngruppeService = lerngruppeService;
    }


    @PostMapping(path = "/registrieren")
    public String registrieren(@RequestBody StudentRequest studentRequest)
    {
        return studentService.registrieren(studentRequest);
    }

    @PostMapping(path = "/login")
    public String login(@RequestBody LoginRequest loginRequest)
    {
        boolean existiertStudent = studentService.login(loginRequest.getEmail(), loginRequest.getPasswort()).isPresent();
        if (existiertStudent)
        {
            return "Anmeldung erfolgreich abgeschlossen";
        }
        throw new IllegalStateException("Nutzer konnte nicht gefunden werden");
    }

    @PostMapping(path = "/erzeugeGruppe/{id}")
    public String erzeugeGruppe(@RequestBody LerngruppeRequest lengruppeRequest, @PathVariable int id)
    {
        return lerngruppeService.erzeugeLerngruppe(lengruppeRequest, id);
    }

    @PostMapping(path = "/hinzufugen/{gruppenId}/{studentenId}")
    public String studentHinzufugen(@PathVariable int gruppenId, @PathVariable int studentenId)
    {
        return lerngruppeService.studentHinzufugen(gruppenId, studentenId);
    }


}
