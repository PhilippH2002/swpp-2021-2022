package com.example.springbootstudent.Repositories;

import com.example.springbootstudent.Entities.Lerngruppe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LerngruppeRepo extends JpaRepository<Lerngruppe, Integer>
{
    Lerngruppe findById(int id);
}
